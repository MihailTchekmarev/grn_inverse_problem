import random
import math

import gep_parameters as gep

import gep_io as io
import gep_gene as gene
import gep_runge_kutta as rk

#filename = "model_test.tab"
filename = "simple_test.tab"
#filename = "obs.tab"
variables, time, observations = io.get_data(filename)
gep.init(variables, time, observations)
#print(gep.variables)
#print(gep.time_value)
#print(gep.observations)

#gene1 = ['Q', '*', '-', '+', 'a', 'b', 'c', 'd', 'a', 'b']
#gene2 = ['*', 'b', '+', 'a', '-', 'a', 'b', 'b', '+', '/', '+', 'b', 'a']

#gep.variables = ["x1", "x2"] # TODO: temp

#gene_1 = ['+', "*", "x2", 2, "x1", "x1", "x2"]
#gene_2 = ['+', "x2", 1]
#chromo = list()
#chromo.append(gene_1)
#chromo.append(gene_2)

value = [1., 0.]
#print(rk.runge_kutta(chromo, value, gep.time_value))
#print(gene.create_random_gene())


def calculate_fitness(x_1, x_2):
    if len(x_1) != len(x_2) or len(x_1[0]) != len(x_2[0]):
        raise TypeError("Matrices must have same dimensions")

    delta = 0.

    for i in range(len(x_1)):
        for j in range(len(x_1[0])):
            try:
                delta += pow(x_1[i][j] - x_2[i][j], 2)
            except OverflowError:
                delta = float('inf')
    delta = pow(delta, 0.5)

    return 1. / (1. + delta)


def copy_chromosome(chromo):
    copy = []

    for elem in chromo:
        copy.append(elem)
    return copy

repo = open("report3.txt","w")

# Main algorithm
population = []

random.seed(30) # TODO: temp

# Create initial population
for i in range(gep.population_size):
    chromosome = []
    for j in range(gep.genes_in_chromosome):
        chromosome.extend(gene.create_random_gene())
    population.append(chromosome)

for count in range(gep.iterations):
    print("iter " + str(count))
    fitness = []
    # Check fitness
    for chromosome in population:
        predicted = rk.runge_kutta(chromosome, gep.observations[0], gep.time_value)
        fitness.append(calculate_fitness(predicted, gep.observations))

    new_population = []
    selection_range = []

    # Selection

    # Clear bad chromosomes
    for n, chromosome in enumerate(population):
        if (fitness[n] != 0.0) and not (math.isnan(fitness[n])):
            selection_range.append([fitness[n], n])

    selection_range.sort(key=lambda x: -x[0])
    print(selection_range[0][0])
    repo.write(str(selection_range[0][0]) + '\n')

    # Elitism
    top_selections = 2 # TODO: global const
    for i in range(top_selections):
        new_population.append(copy_chromosome(population[selection_range[0][1]]))
        #selection_range.pop(0)

    # Roulette
    roulette_selections = gep.population_size - top_selections
    for i in range(roulette_selections):
        max_f = selection_range[0][0]
        while 1:
            n = random.randint(0, len(selection_range) - 1)
            if random.random() < (selection_range[n][0] / max_f):
                new_population.append(copy_chromosome(population[selection_range[n][1]]))
                #selection_range.pop(n)
                break

    population = new_population.copy()

    # Genetic operations

    coding_region_mutation_rate = 0.8
    # Mutation
    n_symbols = len(gep.symbols)
    n_vars = len(gep.variables)
    max_const_value = 10 # TODO: NB magic number
    for chromo in population:
        for i in range(gep.mutations_per_chromosome):
            rnd_gene = random.randint(0, gep.genes_in_chromosome - 1)
            gene_begin = rnd_gene * gep.gene_length
            gene_end = rnd_gene * gep.gene_length + gep.gene_length
            coding_len = gene.coding_region_len(chromo[gene_begin:gene_end])
            if random.random() < coding_region_mutation_rate:
                rnd_pos = random.randint(0, coding_len - 1)
            else:
                if coding_len != gep.gene_length:
                    rnd_pos = random.randint(coding_len, gep.gene_length - 1)
                else:
                    rnd_pos = coding_len - 1

            if rnd_pos < gep.head_length:
                x = random.randint(0, n_symbols - 1)
                if x == 0:
                    chromo[gene_begin + rnd_pos] = random.uniform(0.00001, max_const_value)  # TODO: magic number
                else:
                    chromo[gene_begin + rnd_pos] = gep.symbols[x]
            else:
                x = random.randint(0, n_vars + 1)

                if x == n_vars + 1:
                    chromo[gene_begin + rnd_pos] = random.uniform(0.00001, max_const_value)
                elif x == n_vars:
                    chromo[gene_begin + rnd_pos] = 't'
                else:
                    chromo[gene_begin + rnd_pos] = gep.variables[x]

    selection_range = []
    for i in range(gep.population_size):
        selection_range.append(i)
    current_selection = random.sample(selection_range, k=int(gep.population_size * gep.transposition_probability))

    # Transposition (IS-)
    transposition_segment_len = 3 # TODO: global const
    for num in current_selection:
        rnd_src_pos = random.randint(0, gep.chromosome_length - transposition_segment_len)

        rnd_dst_gene = random.randint(0, gep.genes_in_chromosome - 1)
        rnd_dst_pos = random.randint(0, gep.head_length - transposition_segment_len)

        for i in range(0, transposition_segment_len):
            population[num][rnd_dst_gene * gep.gene_length + rnd_dst_pos + i] = population[num][rnd_src_pos + i]

    for i in current_selection:
        selection_range.remove(i)
    gene_tr_rate = 0.05 # TODO: global const
    current_selection = random.sample(selection_range, k=int(gep.population_size * gene_tr_rate))

    # Transposition (gene-)
    for num in current_selection:
        rnd_src_gene = random.randint(0, gep.genes_in_chromosome - 1)
        rnd_dst_gene = random.randint(0, gep.genes_in_chromosome - 1)

        for i in range(0, gep.gene_length):
            population[num][rnd_dst_gene * gep.gene_length + i] = population[num][rnd_src_gene * gep.gene_length + i]

    selection_range = []
    for i in range(gep.population_size):
        selection_range.append(i)
    single_crossover_rate = 0.3
    current_selection = random.sample(selection_range, k=int(gep.population_size * single_crossover_rate))

    # Crossover (single-point)
    for num, chromo in enumerate(population):
        if random.random() < gep.crossover_probability:
            rnd_chromo = random.randint(0, gep.population_size - 1)
            rnd_pos = random.randint(1, gep.chromosome_length - 1)

            if rnd_chromo == num:
                continue

            strand_1 = []
            strand_2 = []
            for i in range(rnd_pos, gep.chromosome_length):
                strand_1.append(chromo[rnd_pos])
                strand_2.append(population[rnd_chromo][rnd_pos])
                chromo.pop(rnd_pos)
                population[rnd_chromo].pop(rnd_pos)
            chromo.extend(strand_2)
            population[rnd_chromo].extend(strand_1)

    # Crossover (double-point)
    for num, chromo in enumerate(population):
        if random.random() < gep.crossover_probability:
            rnd_chromo = random.randint(0, gep.population_size - 1)
            rnd_gene = random.randint(0, gep.genes_in_chromosome - 1)
            rnd_pos_1 = random.randint(1, gep.chromosome_length - 1)
            rnd_pos_2 = random.randint(1, gep.chromosome_length - 1)

            if rnd_chromo == num:
                continue
            if rnd_pos_2 < rnd_pos_1:
                tmp = rnd_pos_2
                rnd_pos_2 = rnd_pos_1
                rnd_pos_1 = tmp

            strand_1 = []
            strand_2 = []
            for i in range(rnd_pos_1, rnd_pos_2):
                strand_1.append(chromo[rnd_pos_1])
                strand_2.append(population[rnd_chromo][rnd_pos_1])
                chromo.pop(rnd_pos_1)
                population[rnd_chromo].pop(rnd_pos_1)
            for n, elem in enumerate(strand_2):
                chromo.insert(rnd_pos_1 + n, elem)
            for n, elem in enumerate(strand_1):
                population[rnd_chromo].insert(rnd_pos_1 + n, elem)

repo.close()
for i in range(0, 15):
    print(population[i])
