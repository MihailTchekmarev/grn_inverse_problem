import random
import numbers

import gep_parameters as gep


# Find the length of a substring in a gene that codes a mathematical expression
def coding_region_len(gene):
    max_l = len(gene)
    coding_l = 0
    terminals = 1

    while coding_l < max_l:
        if gene[coding_l] == '+' or gene[coding_l] == '-' or gene[coding_l] == '*' or gene[coding_l] == '/' or gene[coding_l] == '^':  # TODO: maybe two sets: unar_oper, binar_oper
            terminals += 1
        elif gene[coding_l] == 'Q':  # TODO: maybe two sets: unar_oper, binar_oper
            terminals += 0
        else:
            terminals -= 1
        coding_l += 1
        if terminals == 0:
            break

    return coding_l


# Get the numerical value of a terminal symbol in an expression
def get_terminal(value, vars_value, time):
    if isinstance(value, str):
        if value == 't':
            return time
        else:
            return vars_value[gep.variables.index(value)]
    else:
        return value


# Calculate the solution of a gene with given variable values
def calculate_gene(gene, vars_value, time):
    coding_len = coding_region_len(gene)
    gene_copy = gene.copy()

    terminal_p = coding_len - 1
    operator_p = coding_len - 1

    if terminal_p == 0:
        return get_terminal(gene_copy[0], vars_value, time)

    while terminal_p != 0:
        while gene_copy[operator_p] not in gep.operators:
            operator_p -= 1
        oper = gene_copy[operator_p]
        if oper == '+':
            b = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            a = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            gene_copy[operator_p] = a + b
        elif oper == '-':
            b = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            a = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            gene_copy[operator_p] = a - b
        elif oper == '*':
            b = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            a = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            gene_copy[operator_p] = a * b
        elif oper == '/':
            b = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            a = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            try:
                gene_copy[operator_p] = a / b
            except ZeroDivisionError:
                gene_copy[operator_p] = float('inf')
        elif oper == '^':
            b = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            a = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            try:
                gene_copy[operator_p] = pow(a, b)
            except OverflowError:
                gene_copy[operator_p] = float('inf')
            except ZeroDivisionError:
                gene_copy[operator_p] = float('inf')
            if not isinstance(gene_copy[operator_p], numbers.Real):
                gene_copy[operator_p] = float('inf')
        elif oper == 'Q':
            a = get_terminal(gene_copy[terminal_p], vars_value, time)
            terminal_p -= 1
            gene_copy[operator_p] = pow(a, 0.5)
    return gene_copy[0]


# Generate random valid gene sequence
def create_random_gene():
    n_symbols = len(gep.symbols)
    n_vars = len(gep.variables)

    gene = list()

    max_const_value = 10 # TODO: magic number

    for i in range(gep.head_length):
        x = random.randint(0, n_symbols - 1)
        if x == 0:
            gene.append(random.uniform(0.00001, max_const_value)) # TODO: magic number
        else:
            gene.append(gep.symbols[x])
    for i in range(gep.tail_length):
        x = random.randint(0, n_vars + 1)
        if x == n_vars + 1:
            gene.append(random.uniform(0.00001, max_const_value))
        elif x == n_vars:
            gene.append('t')
        else:
            gene.append(gep.variables[x])

    return gene
