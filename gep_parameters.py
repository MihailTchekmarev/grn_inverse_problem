variables = []
operators = []
symbols = []

time_value = []
observations = []

max_operands_per_operation = 0

head_length = 0
tail_length = 0
gene_length = 0
chromosome_length = 0
genes_in_chromosome = 0

population_size = 0

iterations = 0

mutations_per_chromosome = 0
transposition_probability = 0
crossover_probability = 0

def init(vars_value, t_value, obs_value):
    global variables
    variables = vars_value

    global time_value
    time_value = t_value

    global observations
    observations = obs_value

    global operators
    #operators = ['+', '-', '*', '/', 'Q', '^'] # TODO: must be parameter
    operators = ['+', '-', '*', '/', '^']

    global symbols
    symbols = ['c']
    symbols.append('t')
    symbols.extend(variables)
    symbols.extend(operators)

    global max_operands_per_operation
    max_operands_per_operation = 2 # TODO: must be parameter

    global head_length
    head_length = 8 # TODO: must be parameter

    global tail_length
    tail_length = head_length * (max_operands_per_operation - 1) + 1

    global gene_length
    gene_length = head_length + tail_length

    global genes_in_chromosome
    genes_in_chromosome = len(variables)

    global chromosome_length
    chromosome_length = gene_length * genes_in_chromosome

    global mutations_per_chromosome
    mutations_per_chromosome = 2

    global transposition_probability
    transposition_probability = 0.1

    global crossover_probability
    crossover_probability = 0.3

    global population_size
    population_size = 60 # TODO: must be parameter

    global iterations
    iterations = 6000
