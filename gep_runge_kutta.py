import gep_parameters as gep
import gep_gene as gene


# Find the predicted values for ODE system using RK4 method
def runge_kutta(chromosome, init_observations, time_value):
    #if len(chromosome) != len(init_observations):
    #    raise TypeError("Number of variables not equal to number of equations")

    predicted_obs = list()
    predicted_obs.append(init_observations)

    iterations = len(time_value) - 1

    for cnt in range(iterations):
        delta_t = time_value[cnt+1] - time_value[cnt]

        if delta_t > 0.2: # TODO: magic number
            #step_size = 0.1 # TODO: magic number
            step_size = 0.5
            subiterations = delta_t // step_size
            remainder = delta_t % step_size

            interim_time = time_value[cnt]
            interim_obs = predicted_obs[cnt]
            for j in range(int(subiterations)):
                interim_obs = runge_kutta_step(interim_obs, chromosome, interim_time, step_size)
                interim_time += step_size
            interim_obs = runge_kutta_step(interim_obs, chromosome, interim_time, remainder)
            predicted_obs.append(interim_obs)
        else:
            predicted_obs.append(runge_kutta_step(predicted_obs[cnt], chromosome, time_value[cnt], delta_t))
    return predicted_obs


def runge_kutta_step(predicted_obs, chromosome, time, delta_t):
    K = list()

    gene_begin = lambda a: a * gep.gene_length
    gene_end = lambda a: a * gep.gene_length + gep.gene_length

    # K1
    K_i = list()
    for i, x in enumerate(predicted_obs):
        K_i.append(gene.calculate_gene(chromosome[gene_begin(i):gene_end(i)], predicted_obs, time))
    K.append(K_i)

    interim_vars = predicted_obs.copy()

    # K2
    for i, x in enumerate(predicted_obs):
        interim_vars[i] = x + 0.5 * K[0][i] * delta_t
    K_i = list()
    for i, x in enumerate(predicted_obs):
        K_i.append(gene.calculate_gene(chromosome[gene_begin(i):gene_end(i)], interim_vars, time + 0.5 * delta_t))
    K.append(K_i)

    # K3
    for i, x in enumerate(predicted_obs):
        interim_vars[i] = x + 0.5 * K[1][i] * delta_t
    K_i = list()
    for i, x in enumerate(predicted_obs):
        K_i.append(gene.calculate_gene(chromosome[gene_begin(i):gene_end(i)], interim_vars, time + 0.5 * delta_t))
    K.append(K_i)

    # K4
    for i, x in enumerate(predicted_obs):
        interim_vars[i] = x + K[2][i] * delta_t
    K_i = []
    for i, x in enumerate(predicted_obs):
        K_i.append(gene.calculate_gene(chromosome[gene_begin(i):gene_end(i)], interim_vars, time + delta_t))
    K.append(K_i)

    # X[i+1]
    new_obs = list()
    for i, x in enumerate(predicted_obs):
        new_obs.append(x + 1 / 6. * (K[0][i] + 2 * K[1][i] + 2 * K[2][i] + K[3][i]) * delta_t)
    return new_obs
