def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def get_data(filename):
    f = open(filename)

    data = f.read().split("\n")
    variables = data[0].split("\t")
    if "t" not in variables:
        raise ValueError("Time variable missing")
    t_index = variables.index("t")
    variables.remove("t")

    data.pop(0)

    time = list()
    observations = list()

    for l in data:
        line = l.split("\t")
        time.append(line[t_index])
        line.pop(t_index)
        observations.append(line)

    for n, x in enumerate(time):
        if not is_number(x):
            raise TypeError("Value must be a number: " + x)
        elif float(x) < 0:
            raise TypeError("Value must be a positive number")
        else:
            time[n] = float(x)

    for i, row in enumerate(observations):
        for j, x in enumerate(row):
            if not is_number(x):
                raise TypeError("Value must be a number: " + x)
            elif float(x) < 0:
                raise TypeError("Value must be a positive number")
            else:
                observations[i][j] = float(x)

    f.close()
    return variables, time, observations
